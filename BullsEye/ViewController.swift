//
//  ViewController.swift
//  BullsEye
//
//  Created by Pascal Huynh on 19/10/14.
//  Copyright (c) 2014 Beyowi. All rights reserved.
//

import UIKit
import QuartzCore

class ViewController: UIViewController {
  
  var currentSliderValue = 0,
      targetValue = 0,
      score = 0,
      round = 0
  @IBOutlet weak var slider: UISlider!
  @IBOutlet weak var targetLabel: UILabel!
  @IBOutlet weak var scoreLabel: UILabel!
  @IBOutlet weak var roundLabel: UILabel!

  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    customizeSliderStyle()
    startNewGame()
    updateLabels()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  @IBAction func showAlert() {
    let difference = abs(currentSliderValue - targetValue)
    var points = 100 - difference
    var title: String
    
    if difference == 0 {
      title = "Perfect!"
      points += 100
    } else if difference < 5 {
      title = "You almost had it!"
      if difference == 1 {
        points += 50
      }
    } else if difference < 10 {
      title = "Pretty good!"
    } else {
      title = "Not even close..."
    }
    score += points
    
    let message = "You scored \(points) points"
    
    let alert = UIAlertController(title: title,
                                  message: message,
                                  preferredStyle: .Alert)
    let action = UIAlertAction(title: "OK",
                               style: UIAlertActionStyle.Default,
                               handler: { action in
                                            self.startNewRound()
                                            self.updateLabels()
                                        })
    alert.addAction(action)
    presentViewController(alert,
                          animated: true,
                          completion: nil)
  }
  
  @IBAction func sliderMoved(slider: UISlider) {
    currentSliderValue = lroundf(slider.value)
  }
  
  @IBAction func startOver() {
    startNewGame()
    updateLabels()
    
    // Animation
    let transition = CATransition()
    transition.type = kCATransitionFade
    transition.duration = 1
    transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
    
    view.layer.addAnimation(transition, forKey: nil)
  }
  
  func startNewRound() {
    // The upper value of 100 is exclusive with arc4random_uniform
    targetValue = 1 + Int(arc4random_uniform(100))
    round += 1
    currentSliderValue = 50
    slider.value = Float(currentSliderValue)
  }
  
  func updateLabels() {
    targetLabel.text = String(targetValue)
    scoreLabel.text = String(score)
    roundLabel.text = String(round)
  }
  
  func startNewGame() {
    score = 0
    round = 0
    startNewRound()
  }
  
  func customizeSliderStyle() {
    let thumbImageNormal = UIImage(named: "SliderThumb-Normal")
    slider.setThumbImage(thumbImageNormal, forState: .Normal)
    
    let thumbImageHighlighted = UIImage(named: "SliderThumb-Highlighted")
    slider.setThumbImage(thumbImageHighlighted, forState: .Highlighted)
    
    let insets = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 14)
    
    let trackLeftImage = UIImage(named: "SliderTrackLeft")
    let trackLeftResizable = trackLeftImage.resizableImageWithCapInsets(insets)
    slider.setMinimumTrackImage(trackLeftResizable, forState: .Normal)
    
    let trackRightImage = UIImage(named: "SliderTrackRight")
    let trackRightResizable = trackRightImage.resizableImageWithCapInsets(insets)
    slider.setMaximumTrackImage(trackRightResizable, forState: .Normal)
  }
}