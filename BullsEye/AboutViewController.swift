//
//  AboutViewController.swift
//  BullsEye
//
//  Created by Pascal Huynh on 22/10/14.
//  Copyright (c) 2014 Beyowi. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
  
  @IBOutlet weak var webView: UIWebView!

  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
    setWebViewContent()
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  @IBAction func close() {
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  func setWebViewContent() {
    if let HTMLFile = NSBundle.mainBundle().pathForResource("BullsEye", ofType: "html") {
      let HTMLData = NSData(contentsOfFile: HTMLFile)
      let baseURL = NSURL.fileURLWithPath(NSBundle.mainBundle().bundlePath)
      webView.loadData(HTMLData, MIMEType: "text/html", textEncodingName: "UTF-8", baseURL: baseURL)
    }
  }
  
  /*
  // MARK: - Navigation
  
  // In a storyboard-based application, you will often want to do a little preparation before navigation
  override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
  // Get the new view controller using segue.destinationViewController.
  // Pass the selected object to the new view controller.
  }
  */
  
}
